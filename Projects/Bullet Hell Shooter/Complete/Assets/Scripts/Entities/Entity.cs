﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [SerializeField] protected float health = 100f;
    [SerializeField] protected float maxHealth= 100f;

    [SerializeField] protected float speed = 10f;

    [SerializeField] protected float damage = 2.5f;

    public virtual void Damage(float amount)
    {
        // Don't do damage when the amount is negative
        if (amount <= 0f) return;

        // Set the health to the new amount or zero
        health = Mathf.Clamp(health - amount, 0f, maxHealth);

        // When there's no health left, this entity will die
        if(health <= 0f)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }
}
